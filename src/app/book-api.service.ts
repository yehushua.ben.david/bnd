import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class BookAPIService {
 

  providerHost = "http://localhost:8080";

  constructor(private http: HttpClient) { }


  addToStock(name: any, author: any) {
    return this.http.post(this.providerHost+"/addBook" , {name:name,author:author})
  }

  buyBook(bookId) {
    return this.http.get(this.providerHost+"/buyBook/"+bookId)
  }
  getAllBook() {
    return this.http.get(this.providerHost + "/stock")
  }
  getBook(search?) {
    if (typeof(search) == "undefined") {
      return this.http.get(this.providerHost + "/books")
    }
    else {
      return this.http.post<any>(this.providerHost + "/search",
        { pattern: search })
    }

  }
}

