import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { BooksComponent } from './books/books.component';
import { StockComponent } from './stock/stock.component';
import { HomeComponent } from './home/home.component';

import {HttpClientModule, HttpHeaders} from '@angular/common/http'
const httpOptions = {
  headers: new HttpHeaders({ 
    'Access-Control-Allow-Origin':'*',
  })
};
@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    BooksComponent,
    StockComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
