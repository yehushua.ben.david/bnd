import { Component, OnInit } from '@angular/core';
import {BookAPIService} from '../book-api.service'

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {
  stock = []
  constructor(private bookAPI:BookAPIService) { }

  updateStock() {
    this.bookAPI.getAllBook().subscribe((data:any[])=>{
      this.stock=data;
    })
  }

  addToStock(name:string,author:string) {
    name=name.trim()
    author=author.trim()
    if ((name=="") || (author==""))  {
      return 
    }
    this.bookAPI.addToStock(name,author).subscribe((data)=>{ 
      this.updateStock() 
    }) 
  }

  ngOnInit(): void {
    this.updateStock()
    setInterval(() =>  this.updateStock(), 10000);
  }

}
