import { Component, OnInit } from '@angular/core';
import {BookAPIService} from '../book-api.service'

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books = []
  searchVal = ""
  constructor(private bookAPI:BookAPIService) { }
  setSearchVal(event) {
    this.searchVal = event.value;
    this.updatebooks()
  }
  updatebooks() {
    this.bookAPI.getBook(this.searchVal).subscribe((data:any[])=>{
      this.books=data;
    })
  }
  buyBook(bookId) {
    this.updatebooks()
    this.bookAPI.buyBook(bookId).subscribe((ok)=>{
      if (ok) {
        alert("it's your")
      } else {
        alert("Sorry too slow")
      }
    })
  }
  ngOnInit(): void {
    this.updatebooks()
    setInterval(() =>  this.updatebooks(), 10000);
  
  }

}
