import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { HomeComponent } from './home/home.component';
import { StockComponent } from './stock/stock.component';

const routes: Routes = [
{path:"",component:HomeComponent},
{path:"books",component:BooksComponent},
{path:"stock",component:StockComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
